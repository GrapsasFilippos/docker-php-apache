PHP_VERSION := apache

PORT_HTTP := 8081
PORT_SSHD := 2221
RECIPE := symfony
WWW_PASS := secret

HOST_STORAGE := /home/user/path/for/host/storage/
HOST_MOUNT := /home/user/path/for/htdocs/
HOST_MOUNT_SITE_AVAILABLE := /home/user/path/for/sites-available
SSH_FS := /home/user/path/to/ssfs/local/mount
LINK_DB_NAME := mariadb
#LINK_DB_NAME1 := mariadb
#LINK_DB_NAME2 := mariadb
#LINK_DB_NAME3 := mariadb

DI_NAME := graphi/php-latest-apache
DI_TAG := 0.3.6
DC_NAME := php-latest-apache
